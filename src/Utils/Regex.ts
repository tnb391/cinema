export const validate = (key: string, value: string) => {
    if (value === '') return `${key} cannot be empty`;
    else {
        const validator = {
            CVV: /\d{3}/,
            CardNumber: /\d{16}/,
            ExpireDate: /\d{4}/,
            Email: /^\S+@\S+\.\S+$/,
            Phone: /\d{10}/,
            Name: /(\s?\w+\s*)+/
        }

        switch (key) {
            case 'Name':
                return !validator.Name.test(value) ? 'Your name must have at least 1 digit' : ''

            case 'CVV':
                return !validator.CVV.test(value) ? 'CVV must be a 3-digit string' : ''

            case 'CardNumber':
                return !validator.CardNumber.test(value) ? 'Card number must be a 12-digit string' : ''

            case 'ExpireDate':
                return !validator.ExpireDate.test(value) ? 'Expire Date must be a 4-digit string' : ''

            case 'Phone':
                return !validator.Phone.test(value) ? 'Phone number must be a 10-digit string' : ''

            case 'Password':
                return /* value.length > 6 ? 'Password must have at least 6 letters' : */ ''

            case 'Email':
                return !validator.Email.test(value) ? 'Email must in the form "email@email.com"' : ''

            default:
                return ''
        }
    }
}

export function objToString(obj: any): string {
    let str = '';
    for (const [p, val] of Object.entries(obj)) {
        str += `${p}: ${val}, `;
    }
    return str;
}