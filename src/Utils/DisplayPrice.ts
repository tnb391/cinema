export const FormatPrice = (cost: number | string, sign = ',') => {
    let str = cost.toString()
    let result = ''
    while (str.length > 0) {
        result = sign + str.substring(str.length, str.length - 3) + result;
        str = str.substring(0, str.length - 3);
    }
    return result.slice(1);
}