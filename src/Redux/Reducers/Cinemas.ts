import eAction from "~/Components/Model/eAction";

const intialState = {
    cinemas: []
}

const AllCinemas = (state = intialState, {payload, type}:eAction) => {
    switch (type) {
        case 'GET_CINEMAS':            
            return { cinemas: payload };
    
        default:
            return state;
    }
}

export default AllCinemas