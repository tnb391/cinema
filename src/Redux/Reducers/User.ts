import eAction from "~/Components/Model/eAction";

const intialState = {
    email: '',
    name: ''
}

const UserReducer = (state = intialState, {payload, type}:eAction) => {
    switch (type) {
        case 'SET_USER':           
            return { ...state, ...payload };
    
        default:
            return state;
    }
}

export default UserReducer