import React from 'react'
import './style.scss'

interface ModalProps {
    className?: string,
    children?: JSX.Element,
    header?: JSX.Element,
    footer?: JSX.Element,
    footerBtns?: JSX.Element,
    center?: boolean,
    width?: string,
    visble: boolean,
    setVisible: (param: boolean) => void,
}

const Modal = (props: ModalProps) => {
    const { visble, setVisible, header, footer, footerBtns, className, center, width } = props;
    const close = () => { setVisible(false) }

    return (
        <div className={`custom-modal ${visble ? 'active' : ''} ${className??""}`}>
            <div className='modal-mask' onClick={close} />
            <div className={`modal-dialog ${width??""} ${center ? 'center' : ''}`}>
                <div className='modal-header'>
                    {
                        header ?? <div className='modal-header-default'>
                            <h3>This is header</h3>
                            <button onClick={close}>&times;</button>
                        </div>
                    }
                </div>
                <div className='modal-body'>
                    {props.children}
                </div>
                {
                    footer ? footer
                        : <div className='modal-footer'>
                            <h3>This is footer</h3>
                            {
                                footerBtns ? footerBtns
                                    : <div className='modal-footer-btns'>
                                        <button onClick={close}>Cancel</button>
                                        <button>Accept</button>
                                    </div>
                            }
                        </div>
                }
            </div>
        </div>
    )
}

export default React.memo(Modal)