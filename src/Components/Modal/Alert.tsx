import { faTimesCircle } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

type eAlert = { 
    type: string, 
    content: string,
    callback: any 
}

const Alert = ({ type, content, callback }: eAlert) => (
    <div className={`alert ${type}`}>
        <p className="text-end close">
            <FontAwesomeIcon 
                icon={faTimesCircle} 
                color='tomato' 
                fontSize={16} 
                onClick={()=>callback(false)}
            />
        </p>
        <p className="px-2">{content}</p>
    </div>
);

export default Alert;