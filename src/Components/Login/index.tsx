import { useSelector } from 'react-redux';
import { Navigate } from 'react-router-dom'
import Login from './Login'

const LoginAuth = () => {
    const {name} = useSelector((state:any) => {
        return state.UserReducer;
    });
    if (name) 
        return <Navigate to='/user' />
    return <Login />
}

export default LoginAuth