import React, { useState, useContext, useCallback } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';

import { getCookie } from '~/Utils/Cookies';
import { objToString } from '~/Utils/Regex';
import { eUser, unknowUser } from '~/Components/Model/eUser';
import { eTicket } from '~/Components/Model/eTicket';
import { BookingContext } from '..';
import { NotAvailable } from './NewCard';
import { ticketTime } from '~/Utils/Timing';
import AvailableCard from './AvailableCard';

function Payment({ back }: any) {
    const { info } = useSelector((state: any) => state.AllBooking);
    const dispatch = useDispatch();
    const ctx = useContext(BookingContext) as any;
    const { id } = useParams()

    const [show, setShow] = useState<boolean>(true)
    const [user, setUser] = useState<eUser>(() => {
        const user = getCookie('user')
        return (user) ? { ...unknowUser, ...JSON.parse(user) } : unknowUser;
    })

    const changeUserInfo = useCallback((key: string, value: string) => {
        setUser({ ...user, [key]: value })
    }, [user])


    const submit = (e: any) => {
        e.preventDefault();
        const data = {
            BankId: user.bankId * 1 + 1,
            CardNumber: user.cardNumber,
            CardName: user.name,
            ExpireDate: user.expDate,
            CVV: user.cvv,
            Price: ctx.cost1 + ctx.cost2,
            ShowCode: id,
            Email: user.email,
            FilmName: info.movie.name,
            CinemaName: info.cinema,
            TheaterName: info.theater,
            Combo: objToString(ctx.combo),
            SeatCode: ctx.seats.join(', '),
            ShowTime: `${ticketTime(info.showdate.substring(info.showdate.length - 10))}T${info.showtime}Z`,
            ImageLandscape: info.movie.imgLandscape,
            ImagePortrait: info.movie.imgPortrait,
        } as eTicket;

        dispatch({ type: 'POST_TICKETS', payload: { data, callback: ctx.setSucess } })
    }

    return (
        <div className='payment'>
            <h2 className='color_white py_1'>Thanh toán</h2>
            <form onSubmit={submit}>
                <div>
                    {/* <div className='payment-input'>
                        <label htmlFor="">Email</label>
                        <input
                            value={user.email}
                            onChange={(e) => changeUserInfo('email', e.target.value)}
                            required
                        />
                        <div className='error-msg'>
                            <span>{validate('Email', user.email)}</span>
                        </div>
                    </div> */}
                    <div className='bankcards'>
                        <div className='d_flex'>
                            <h3
                                className={`px_1 m_1-r ${show ? 'active' : ''}`}
                                onClick={() => setShow(true)}
                            >
                                Chọn thẻ có sẵn
                            </h3>
                            <h3
                                className={`px_1 ${!show ? 'active' : ''}`}
                                onClick={() => setShow(false)}
                            >
                                Liên kết thẻ mới
                            </h3>
                        </div>
                        {
                            show && <AvailableCard user={user} callback={changeUserInfo} />
                        }
                        {
                            // !show && <NewCard user={user} callback={changeUserInfo} />
                            !show && <NotAvailable user={user} callback={changeUserInfo}/>
                        }
                    </div>
                </div>
                <div>
                    <p className='font-sm py_2'>
                        (*) Bằng việc click vào THANH TOÁN, bạn đã xác nhận hiểu rõ các {' '}
                        <Link to='/terms'>Quy định Giao dịch Trực tuyến</Link>
                        {' '} của chúng tôi.
                    </p>
                    <div className='d_flex jus_space'>
                        <button className='w_33 p_1' onClick={() => back(false)}>
                            Back
                        </button>
                        <button type='submit' className='submit w_33'>Thanh toán</button>
                    </div>
                </div>
            </form>
        </div>
    )
}

export default React.memo(Payment)