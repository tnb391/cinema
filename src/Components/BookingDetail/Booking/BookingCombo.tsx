import React, { useState, useEffect, useContext } from 'react'
import { useSelector } from "react-redux";
import { faMinusCircle, faPlusCircle } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { FormatPrice } from '../../../Utils/DisplayPrice';
import { BookingContext } from '..';

function BookingCombo() {
    const [num, setNum] = useState([0, 0, 0, 0])
    const [total, setTotal] = useState(0)
    const { setCost2, combo, setCombo } = useContext(BookingContext) as any;
    const { booking } = useSelector((state: any) => state.AllBooking);

    const transvalue = (type: string, index: number) => {
        const name = booking.consession[0].concessionItems[index].description;
        if (type === 'add') {
            setNum(num.map((c, i) => i === index ? c + 1 : c));
            setCombo({ ...combo, [name]: combo[name] + 1 })
        }
        else if (type === 'sub') {
            setNum(num.map((c, i) => i === index ? (c < 1 ? 0 : c - 1) : c));
            setCombo({ ...combo, [name]: combo[name] > 1 ? combo[name] - 1 : 0 })
        }
    }

    const handleChange = (event: any, i: number) => {
        const amount = parseInt(event.target.value);
        const distance: number = amount - num[i];
        const name = booking.consession[0].concessionItems[i].description;
        if (distance > 0) {
            setNum(num.map((c, index) => i === index ? amount : c))
            setCombo({ ...combo, [name]: amount })
        }
        else {
            setNum(num.map((c, index) => i === index ? 0 : c))
            setCombo({ ...combo, [name]: amount < 1 ? amount : 0 })
        }
    }

    useEffect(() => {
        if (booking?.consession && booking.consession[0].concessionItems) {
            const sum = num.reduce((sum, n, i) => sum + n * booking.consession[0].concessionItems[i].displayPrice, 0);
            setTotal(sum);
            setCost2(sum);
        }
    }, [num, booking, setCost2])

    return (
        <table className='bookingTable'>
            <thead className='font-lg'>
                <tr>
                    <th className='w_50 tableLs text-left'>Combo</th>
                    <th className='tableLs text-center' >Số lượng</th>
                    <th className='tableLs text-center' >Giá (VNĐ)</th>
                    <th className='tableLs text-end' >Tổng (VNĐ)</th>
                </tr>
            </thead>

            <tbody>
                {
                    booking?.consession
                    && booking.consession[0]?.concessionItems.map((n: any, i: number) => {
                        return <tr className='lsBooking' key={i}>
                            <td className='tableLs w_50 text-left'>
                                <div className='d_flex'>
                                    <img src={n.imageUrl} alt=''/>
                                    <div className='m_1-l'>
                                        <h4>{n.description}</h4>
                                        <p className='font-sm'>{n.extendedDescription}</p>
                                    </div>
                                </div>
                            </td>
                            <td className='tableLs'>
                                <div className='d_flex jus_space align_center'>
                                    <FontAwesomeIcon
                                        onClick={() => transvalue('sub', i)}
                                        className='icon-24 fIcon'
                                        icon={faMinusCircle}
                                    />
                                    <input
                                        value={num[i]}
                                        type={'number'}
                                        onChange={(e) => handleChange(e, i)}
                                    />
                                    <FontAwesomeIcon
                                        onClick={() => transvalue('add', i)}
                                        className='icon-24 fIcon'
                                        icon={faPlusCircle}
                                    />
                                </div>
                            </td>

                            <td className='tableLs text-center'>
                                <p> {FormatPrice(n.displayPrice)}</p>
                            </td>

                            <td className='tableLs text-end'>
                                {FormatPrice(n.displayPrice * num[i])}
                            </td>
                        </tr>
                    })
                }
                <tr className='border-t'>
                    <td
                        style={{ fontWeight: '600', color: 'rgb(255, 118, 69)' }}
                        className='tableLs text-left'
                        colSpan={3}
                    >
                        Tổng
                    </td>
                    <td className='tableLs text-right'>
                        {FormatPrice(total)}
                    </td>
                </tr>
            </tbody>
        </table>
    )
}

export default React.memo(BookingCombo);
