import React from 'react';
import './rules.scss';

function Term() {
    return (
        <div className="backgroundsize">
            <div className="mainSizeRule main_width m_auto">
                <div className="infor">
                    <h1>Quy định giao dịch trực tuyến của Rạp chiếu phim</h1>
                    <div className="facerule"></div>
                    <div>
                        <h4 className="m_1-b">
                            {' '}
                            Điều kiện và điều khoản của chương trình Đặt vé trực
                            tuyến{' '}
                        </h4>
                        <p className="m_1-b">
                            Galaxy Cinema xin chân thành cảm ơn quý khách hàng
                            đã tin tưởng lựa chọn mua vé trực tiếp tại trang web
                            cũng như ứng dụng di động của Galaxy Cinema. Để quy
                            trình mua vé thuận thiện, và đảm bảo quý khách hiểu
                            rõ mọi thông tin, quy định, chính sách mua vé trực
                            tuyến của Galaxy Cinema, xin quý khách lưu ý các
                            điều khoản dưới đây:
                        </p>
                        <p className="m_1-b">
                            <p className="m_1-b">
                                Chương trình Đặt vé trực tuyến (Booking Online)
                                được dành riêng cho khách hàng của Galaxy Cinema
                                và được áp dụng tại hệ thống rạp chiếu phim
                                Galaxy Cinema. Ngoài số tiền phải thanh toán
                                trên số lượng vé và phần đồ ăn thức uống được
                                mua, khách hàng không phải trả thêm bất kỳ khoản
                                chi phí nào khác.{' '}
                            </p>
                            <p className="m_1-b">
                                Mỗi mỗi khách hàng Galaxy Cinema được tham gia
                                đặt vé MỘT LẦN cho một bộ phim, tối đa là 8 vé
                                cho một lần đặt. Vé xem phim được đặt trước từ
                                website Galaxy Cinema sẽ được giữ riêng cho bạn
                                cho đến hết thời hạn xuất vé. Mỗi bộ phim có
                                thời hạn xuất vé khác nhau. Nếu bạn không đến
                                lấy vé trong thời gian qui định, hệ thống sẽ tự
                                động khóa lệnh đặt vé của bạn và bạn sẽ không
                                thể lấy vé được, và Galaxy Cinema sẽ không hoàn
                                trả bất kỳ chi phí nào cho bạn.{' '}
                            </p>
                            <p>
                                Vui lòng sắp xếp thời gian đến rạp chiếu phim 45
                                phút trước suất chiếu để thuận tiện cho việc lấy
                                vé.
                            </p>
                            <p>
                                Khách hàng phải đảm bảo mua vé phim cho người có
                                độ tuổi phù hợp theo quy định của Cục Điện Ảnh
                                Việt Nam về Bảng Tiêu Chí Phân Loại Phổ Biến
                                Phim Theo Độ Tuổi, như sau:
                            </p>
                            <ul>
                                <li>
                                    <p>P: Phim phổ biến cho mọi đối tượng.</p>
                                </li>
                                <li>
                                    <p>C13: Phim cấm khán giả dưới 13 tuổi.</p>
                                </li>
                                <li>
                                    <p>C16: Phim cấm khán giả dưới 16 tuổi.</p>
                                </li>
                                <li>
                                    <p>C18: Phim cấm khán giả dưới 18 tuổi.</p>
                                </li>
                                <li>
                                    <p>
                                        Lưu ý: Ở mỗi trang phim, Galaxy Cinema
                                        luôn để biểu tượng thể hiện Phân Loại
                                        Phổ Biến Phim (C13, C16, C18) tại mỗi
                                        trang phim, khách hàng vui lòng kiểm tra
                                        kỹ trước khi lựa chọn đặt vé trực tuyến.
                                    </p>
                                </li>
                                <li>
                                    <p>
                                        Những phim không có biểu tượng thể hiện
                                        Phân Loại Phổ Biến Phim (C13, C16, C18)
                                        nghĩa là phim phổ biến cho mọi khán giả.
                                    </p>
                                </li>
                            </ul>
                            <p>
                                Hiện tại chúng tôi chưa có dịch vụ hủy hoặc thay
                                đổi thông tin vé đã đặt hoặc đã được thanh toán
                                trực tuyến (thông tin phim, suất chiếu, loại
                                vé...). Vì vậy, rất mong các bạn kiểm tra cẩn
                                thận các thông tin vé trước khi tiến hành xác
                                nhận đặt vé và thanh toán. Đồng thời, trong
                                trường hợp đã đặt vé và thanh toán trực tuyến,
                                nếu bạn bỏ suất chiếu bạn sẽ không được hoàn chi
                                phí.
                            </p>
                            <p>
                                Sau khi hoàn thành việc thanh toán vé trực
                                tuyến, bạn sẽ nhận được thư xác nhận thông tin
                                chi tiết vé đã đặt thông qua địa chỉ thư điện tử
                                (email) mà bạn đã cung cấp. Đồng thời, chúng tôi
                                cũng sẽ gửi tin nhắn (SMS) thông báo mã vé điện
                                tử tới số điện thoại bạn đăng ký. Chúng tôi
                                không chịu trách nhiệm trong trường hợp bạn nhập
                                sai địa chỉ email, số điện thoại và thông tin cá
                                nhân khác dẫn đến không nhận được thư xác nhận.
                                Vì vậy, vui lòng kiểm tra lại chính xác các
                                thông tin trước khi thực hiện thanh toán. Lưu ý
                                là email và SMS này chỉ có tính chất xác nhận
                                thông tin vé sau khi bạn đã đặt vé thành công.
                                Do đó, chúng tôi đề nghị bạn đọc kĩ các thông
                                tin trên màn hình về rạp chiếu phim, tên phim,
                                suất chiếu, và chỗ ngồi trước khi hoàn tất việc
                                xác nhận tất cả các thông tin về vé. Email xác
                                nhận thông tin đặt vé có thể đi vào hộp thư rác
                                của bạn, vì vậy hãy kiểm tra chúng trước khi
                                liên lạc với chúng tôi.
                            </p>
                            <p>
                                Sau 30 phút kể từ khi bạn thanh toán thành công
                                mà vẫn chưa nhận được bất kỳ xác nhận nào (qua
                                email hoặc SMS), vui lòng liên hệ Galaxy Cinema
                                theo email: supports@galaxy.com.vn, hoặc gọi đến
                                hotline 1900 2224 (Lưu ý: Hotline hoạt động mỗi
                                ngày từ 8h – 22h) để được hỗ trợ. Nếu bạn không
                                liên hệ lại coi như Galaxy Cinema đã hoàn thành
                                nghĩa vụ với bạn.
                            </p>
                            <p>
                                Vui lòng đến rạp mà bạn đã chọn trên website để
                                lấy vé. Khi đến lấy vé, bạn cần xuất trình mã
                                đặt vé. Trong trường hợp Galaxy Cinema cần đối
                                chiếu thông tin, các bạn cần xuất trình một
                                trong các giấy tờ tùy thân: CMND, hộ chiếu, bằng
                                lái xe, thẻ học sinh sinh viên còn hiệu lực.
                            </p>
                            <p>
                                Bằng việc đặt vé qua website này, bạn đồng ý
                                rằng bạn sẽ chấp nhận ghế đã đặt. Bạn đồng ý
                                rằng, trong những trường hợp có sự thay đổi về
                                chương trình phim hoặc bất khả kháng, chúng tôi
                                có quyền từ chối bán vé cho bạn hoặc rút lại bất
                                kỳ vé nào từ việc mua bán qua trang web này.
                            </p>
                            <p>
                                Galaxy Cinema có quyền tạm ngưng, hoãn hoặc thay
                                đổi điều khoản của chương trình Đặt vé trực
                                tuyến (Booking Online) tại bất kì thời điểm nào
                                mà không cần báo trước.
                            </p>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Term;
