const StandarSeats = ({ arr, pick, picked, sold }: any) => {
    const isPicked = (id: string, letter: string) => {
        const pos = letter + id;
        if (sold.indexOf(pos) === -1)
            return picked.indexOf(pos) === -1 ? '' : 'picked';
        return 'sold';
    }

    return (
        <div className='seat-standard'>
            {
                arr && arr.map((row: any, index: number) => {
                    return !row.physicalName ? <div key={index} className='seat-row empty' />
                        : <div key={index} className='seat-row'>
                            <p className='d_flex align_center'>{row.physicalName}</p>
                            <div className='seats'>
                                {
                                    row.seats.map((letter: any) =>
                                        <span
                                            className={`${isPicked(letter.id, row.physicalName)}`}
                                            onClick={() => { pick(row.physicalName, letter.id) }}
                                            key={letter.id}
                                        >
                                            {letter.id}
                                        </span>
                                    )
                                }
                            </div>
                            <p className='d_flex align_center'>{row.physicalName}</p>
                        </div>
                })
            }
        </div>
    )
}

export default StandarSeats