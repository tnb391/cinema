
import { faCheckCircle, faHandsClapping } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Link } from 'react-router-dom'

import Promotion from '~/Components/Promotion'
import AboutFilm from '../AboutFilm/aboutFilm'
import './style.scss'

const Success = () => {
    return (
        <div className='successful'>
            <div className='py_5 main_width m_auto'>
                <div>
                    <div className='sucHead text-center border rounded-md'>
                        <div className='icon-check m_auto'>
                            <FontAwesomeIcon icon={faCheckCircle} fontSize={80} color='limegreen' />
                        </div>
                        <div className='sucContent p_2'>
                            <h1 className='w_100'>Chúc mừng! Bạn đã đặt vé thành công!</h1>
                            <p>
                                <FontAwesomeIcon icon={faHandsClapping} />
                                {' '}
                                Cám ơn bạn đã sử dụng dịch vụ của chúng tôi, hẹn gặp bạn trong suất chiếu tới!
                                {' '}
                                <FontAwesomeIcon icon={faHandsClapping} />
                            </p>
                            <Link to='/'>
                                Nhấn vào đây để quay lại trang chủ
                            </Link>
                            <div className='sucImg'>
                                <div className='ball'></div>
                                <div className='ball2'></div>
                                <div className='ball3'></div>
                            </div>
                        </div>
                    </div>
                </div>
                <AboutFilm isComplete={true} />
            </div>
            <Promotion />
        </div>
    )
}

export default Success