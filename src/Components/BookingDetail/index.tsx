import { useDispatch, useSelector } from 'react-redux';
import { useEffect, createContext, useState, useContext, useMemo } from 'react';
import { useSearchParams } from 'react-router-dom';

import './style.scss'
import BookingCombo from './Booking/BookingCombo';
import BookingTickets from './Booking/BookingTickets';
import AboutFilm from './AboutFilm/aboutFilm';
import Seat from './Seat';
import Modal from '../Modal';
import eMovie from '../Model/eMovie';
import Success from './Success';

export const BookingContext = createContext({})
export const useBookingContext = () => useContext(BookingContext);

function Booking() {
    const [cost1, setCost1] = useState<number>(0);
    const [cost2, setCost2] = useState<number>(0);
    const [seats, setSeats] = useState<Array<string>>([]);
    const [combo, setCombo] = useState({});
    const [show, setShow] = useState(false);
    const [sucess, setSucess] = useState(false);
    const [params] = useSearchParams();
    const dispatch = useDispatch();

    const movieId = params.get("mid");
    const { comingMovies, showingMovies } = useSelector((state: any) => {
        return state.AllMovies;
    })

    let movie = useMemo(() => {
        return showingMovies.find((m: eMovie) => m.id === movieId) || comingMovies.find((m: eMovie) => m.id === movieId);
    }, [movieId, comingMovies, showingMovies]) as eMovie;

    useEffect(() => { dispatch({ type: 'GET_Booking' }) }, [dispatch]);
    useEffect(() => {
        if (comingMovies.length === 0 && showingMovies.length === 0) {
            dispatch({ type: 'GET_ALL' })
        }
    }, [comingMovies, showingMovies, dispatch])

    const value = {
        cost1, setCost1,
        cost2, setCost2,
        combo, setCombo,
        seats, setSeats,
        setShow, show,
        movie,
        setSucess
    }

    return (
        <BookingContext.Provider value={value}>
            {sucess ? <Success />
                : <div className='bookingMainSize'>
                    <div className='bookingSize py_5 main_width m_auto'>
                        <h1 className='color_white'>Mua vé/Đặt thức ăn</h1>
                        <div className='bookingDetail'>
                            <div className='bookingDetial_content'>
                                <BookingTickets />
                                <BookingCombo />
                            </div>
                            <AboutFilm isComplete={false} />
                        </div>
                    </div>
                    {
                        show && <Modal
                            visble={show}
                            setVisible={setShow}
                            className='main_width m_auto'
                            width='w_75'
                            header={<></>}
                            footer={<></>}
                        >
                            <Seat />
                        </Modal>
                    }
                </div>
            }
        </BookingContext.Provider>
    )
}

export default Booking;
