import React from 'react'
import './loadHome.scss'

function loadHome() {
    return (
        <div className='Loading'>
            <div className='LoadingSize'>
            </div>
        </div>
    )
}

export default loadHome;