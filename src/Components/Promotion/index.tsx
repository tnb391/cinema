import './style.scss'

function Promotion() {
	const pic1 = require('./img/promotion1.jpg')
	const pic2 = require('./img/promotion2.jpg')
	const pic3 = require('./img/promotion3.jpg')
	const pic4 = require('./img/promotion4.jpg')
	const pic5 = require('./img/promotion5.jpg')

	const picture = [pic1, pic2, pic3, pic4, pic5]

	return (
		<div className='backGroundPro'>
			<div className='mainSizePro main_width m_auto'>
				<h2 className='py_2'>Khuyến mại</h2>
				<div className='d_flex flex-gap-2 py_2'>
					{
						picture.map((n: any, i: number) =>
							<div key={i} className='promotion-card'>
								<img className='w_100' src={n} alt='' />
								<div className='d_flex jus_center'>
									<button> Xem chi tiết </button>
								</div>
							</div>
						)
					}
				</div>
			</div>
		</div>

	)
}

export default Promotion;