import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from "react-redux";
import ShowingMovie from '../Movie/ShowingMovie';
import './AllMovies.scss'

function Showing() {
    const allMovies = useSelector((state: any) => state.AllMovies);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch({ type: 'GET_ALL' })
    }, [dispatch]);

    let [checked, setChecked] = useState<boolean>(true);

    return (
        <div className='background'>
            <div id='all-movies' className='main_width m_auto'>
                <div className="tabs">
                    <div className="tab__btns">
                        <button
                            className={checked ? "tab__btn tab__btn--active" : "tab__btn"}
                            data-item="item-1"
                            onClick={() => setChecked(true)}
                        >
                            <span><h3>Showing Movie</h3></span>
                        </button>
                        <button
                            className={checked ? "tab__btn" : "tab__btn tab__btn--active"}
                            data-item="item-2"
                            onClick={() => setChecked(false)}
                        >
                            <span><h3>Coming Movie</h3></span>
                        </button>
                    </div>

                    <div className="tab__items">
                        <div
                            id="item-1"
                            className={"py_5 tab__item" + (checked ? 'tab__item--active' : '')}
                        >
                            <ShowingMovie movielist={allMovies.showingMovies} />
                        </div>
                        <div
                            id="item-2"
                            className={"py_5 tab__item" + (checked ? '' : 'tab__item--active')}
                        >
                            <ShowingMovie movielist={allMovies.comingMovies} />
                        </div>
                    </div>
                </div>


            </div>
        </div>
    )
}

export default Showing