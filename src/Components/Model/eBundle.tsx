import eSession from "./eSession"
export default interface eBundle {
    version: string,
    caption: string,
    sessions: Array<eSession>,
    code: string
}