export default interface eCity {
	name: string,
	slug: string,
	district: Array<string>,
	id: string
}

export const nullCity : eCity = {
	name: '',
	slug: '',
	district: [],
	id: ''
}