import eBundle from "./eBundle"
export default interface eDate {
    showDate: string,
    dayOfWeekLabel: string,
    bundles : Array<eBundle>
}