export interface eTicket {
    BankId: number,
    CardNumber: string, // "0000000000000000",
    CardName: string,  //"Nguyen Van A",
    ExpireDate: string //"0124",
    CVV: string, //"888",
    Price: number,
    ShowCode: string, //"cinemaId-sessionId",
    Email: string, // "user@example.com",
    CinemaName: string, // "string",
    TheaterName: string, // "string",
    Combo: string, // "string",
    SeatCode: string, // "string",
    ShowTime: string, // "yyyy-MM-dd HH:mm"
    FilmName: string,
    ImageLandscape: string,
    ImagePortrait: string,
}