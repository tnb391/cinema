import eDate from "./eDate"

export default interface eMovie  {
    id: string,
    age: string
    name: string,
    imageLandscape: string,
    imageLandscapeMobile: string,
    imagePortrait: string,
    subName: string,
    point: number,
    totalVotes: number,
    description: any,
    duration: string,
    startdate: string,
    trailer: string,
    slug: string,
    dates: Array<eDate>
}