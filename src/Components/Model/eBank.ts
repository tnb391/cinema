export interface eBank {
    Id: number,
    Name: string,
    Logo: string,
    CardNumber: string
}

export interface eCard {
    Id: number,
    Name: string,
    Logo: string,
    CardNumber: string
}

export const emptyCard = {
    Id: 0,
    Name: '',
    Logo: '',
    CardNumber: ''
}

export const banks = [
    {
        "Id": 1,
        "Name": "TP Bank",
        "Logo": "https://cdn.haitrieu.com/wp-content/uploads/2022/02/Icon-TPBank.png"
    },
    {
        "Id": 2,
        "Name": "Agribank",
        "Logo": "https://seeklogo.com/images/A/agribank-2014-logo-D607F59D1E-seeklogo.com.png"
    }
]