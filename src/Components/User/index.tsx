import { useSelector } from 'react-redux'
import { Navigate } from 'react-router-dom'
import User from './User'

const UserAuth = () => {
    const { name } = useSelector((state: any) => {
        return state.UserReducer
    });
    
    if (name) return <User />
    return <Navigate to='/login' />
}

export default UserAuth