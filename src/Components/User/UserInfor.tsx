import { useRef, useState } from 'react';
import { useDispatch } from 'react-redux';

import { eUser } from '../Model/eUser';
import { validate } from '~/Utils/Regex';
import { showPassword } from '~/Utils/ToogleInputPassword';
import Alert from '../Modal/Alert';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEye, faEyeSlash } from '@fortawesome/free-solid-svg-icons';

const UserInfor = ({ user, callback }: { user: eUser; callback: any }) => {
    const [error, setError] = useState<Array<string>>([]);
    const [changePassword, setChangePassword] = useState(false);
    const [errorMsg, setErrorMsg] = useState(false);
    const [successMsg, setSuccessMsg] = useState(false);
    const dispatch = useDispatch();
    const password = useRef<HTMLInputElement>(null);
    const newpass1 = useRef<HTMLInputElement>(null);
    const newpass2 = useRef<HTMLInputElement>(null);

    const hideMessages = () => {
        setErrorMsg(false);
        setSuccessMsg(false)
    }

    const submit = async (e: any) => {
        e.preventDefault();
        const pass = password.current?.value ?? '';
        const pass1 = newpass1.current?.value ?? '';
        const pass2 = newpass2.current?.value ?? '';
        const nameCheck = validate('Name', user.name);
        const emailCheck = validate('Email', user.email);
        const passCheck = validate('Password', pass);

        if ((pass1 || pass2) && passCheck === '') { // when user want to change password
            if (pass1 === pass2) {
                dispatch({
                    type: 'UPDATE_PASSWORD',
                    payload: {
                        data: [user.email, pass, pass1],
                        setSuccessMsg,
                        setErrorMsg
                    },
                })
            }
            else setErrorMsg(true);
        }

        if (nameCheck === '' && emailCheck === '' && passCheck === '') {
            dispatch({
                type: 'UPDATE_USER',
                payload: {
                    data: [user.email, user.name, pass],
                    setErrorMsg,
                    setSuccessMsg
                },
            });
        }

        setError([nameCheck, emailCheck, passCheck]);
    };

    return (
        <>
            <form onSubmit={submit}>
                <div className="">
                    <label>Họ & Tên:</label>
                    <div>
                        <input
                            placeholder="Họ và Tên..."
                            value={user.name}
                            onChange={(e) => callback('name', e.target.value)}
                            onFocus={() => hideMessages()}
                            className='border rounded-sm'
                        />
                        <p className="error-msg">{error[0]}</p>
                    </div>
                </div>
                <div className="">
                    <label>Email:</label>
                    <div>
                        <input
                            placeholder="Vd: email@email.com."
                            value={user.email}
                            onChange={(e) => callback('email', e.target.value)}
                            onFocus={() => hideMessages()}
                            className='border rounded-sm'
                            readOnly
                        />
                        <p className="error-msg">{error[1]}</p>
                    </div>
                </div>
                <div>
                    <label>Mật khẩu:</label>
                    <div>

                        <div className='d_flex border rounded-sm'>
                            <input
                                type="password"
                                ref={password}
                                onFocus={() => hideMessages()}
                                name='password'
                            />
                            <button
                                className='bg-none border-none'
                                onClick={() => showPassword('password')}
                                type='button'
                            >
                                {
                                    password.current?.type === 'text' ?
                                        <FontAwesomeIcon icon={faEye} /> : <FontAwesomeIcon icon={faEyeSlash} />
                                }
                            </button>
                        </div>
                        <p className="error-msg">{error[2]}</p>
                    </div>
                </div>
                {!changePassword ? (
                    <div>
                        <div></div>
                        <p
                            className="pointer"
                            onClick={() => setChangePassword(true)}
                        >
                            Đổi mật khẩu ?
                        </p>
                    </div>
                ) : (
                    <div>
                        <div>
                            <label>Đổi mật khẩu</label>
                            <p
                                className="pointer m_1"
                                onClick={() => setChangePassword(false)}
                            >
                                Ẩn
                            </p>
                        </div>
                        <div className="d_flex gap-1">
                            <div className="w_50">
                                <label className="font-sm">Mật khẩu mới</label>
                                <div className='d_flex border rounded-sm'>
                                    <input ref={newpass1} name='newpass' type='password' />
                                    <button
                                        className='bg-none border-none'
                                        onClick={() => showPassword('newpass')}
                                        onFocus={() => hideMessages()}
                                        type='button'
                                    >
                                        {
                                            newpass1.current?.type === 'text' ?
                                                <FontAwesomeIcon icon={faEye} /> : <FontAwesomeIcon icon={faEyeSlash} />
                                        }
                                    </button>
                                </div>
                            </div>
                            <div className="w_50">
                                <label className="font-sm">
                                    Nhập lại mật khẩu
                                </label>
                                <div className='d_flex border rounded-sm'>
                                    <input ref={newpass2} name='renewpass' type='password' />
                                    <button
                                        className='bg-none border-none'
                                        onClick={() => showPassword('renewpass')}
                                        onFocus={() => hideMessages()}
                                        type='button'
                                    >
                                        {
                                            newpass2.current?.type === 'text' ?
                                                <FontAwesomeIcon icon={faEye} /> : <FontAwesomeIcon icon={faEyeSlash} />
                                        }
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                )}
                <div>
                    <div></div>
                    <input
                        className="submit-btn"
                        type="submit"
                        value="Lưu thay đổi"
                    />
                </div>
            </form>
            {errorMsg && 
                <Alert content="Email hoặc mật khẩu không đúng!" type='error' callback={setErrorMsg}/>}
            {successMsg && 
                !errorMsg && 
                    <Alert content="Cập nhật thông tin thành công!" type='success' callback={setSuccessMsg}/>}
        </>
    );
};

export default UserInfor;
