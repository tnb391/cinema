import eDate from "../Model/eDate";
import eBundle from "../Model/eBundle";
import eSession from "../Model/eSession";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";

interface eProps {
    movie: any,
    date: string,
    name: string
}

const MovieInDay = ({ movie, date, name }: eProps) => {
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const handleBooking = (m: any, s: eSession) => {
        const sd: eDate = m.dates.find((d: eDate) => d.showDate === s.showDate);
        const data = {
            movie: {
                imgLandscape: m.imageLandscape,
                imgPortrait: m.imagePortrait,
                name: m.name,
                subName: m.subName,
                age: m.age
            },
            cinema: `${name}`,
            theater: `${s.screenName}`,
            showdate: `${sd.dayOfWeekLabel}, ${sd.showDate}`,
            showtime: s.showTime,
        }
        dispatch({ type: 'SET_BOOKING', payload: data })
        navigate(`/booking/${s.id}?mid=${movie.id}`)
    }

    if (movie.dates) {
        let match = movie.dates.find((d: eDate) => d.showDate === date);
        if (match && match.bundles) {
            return (
                <>
                    {
                        match.bundles.map((b: eBundle) =>
                            <div key={b.code} className='showtime py_2'>
                                <div>
                                    {b.version} - {b.caption === 'sub' ? 'Phụ đề' : 'Lồng tiếng'}
                                </div>
                                <div className="flex-gap-1">
                                    {b.sessions.map((sess: eSession) =>
                                        <span
                                            onClick={() => handleBooking(movie, sess)}
                                            className="time"
                                            key={sess.id}
                                        >
                                            {sess.showTime}
                                        </span>
                                    )}
                                </div>
                            </div>
                        )
                    }
                </>
            )
        }
    }
    return <div className="py_1"></div>
}

export default MovieInDay;