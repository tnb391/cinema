import { Link, redirect } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux';
import './style.scss'
import { getCookie, removeCookie } from '~/Utils/Cookies';
const Logo = require('~/Assets/Images/logo.png')

export default function Header() {
    const user = getCookie('user');
    const dispatch = useDispatch();
    const temp = useSelector((state: any) => state.UserReducer);
    const name = user ? JSON.parse(user).name : temp.name;

    const logOut = () => {
        removeCookie('user');
        dispatch({
            type: 'SET_USER',
            payload: { email: '', name: '' }
        })
        redirect('/')
    }

    return (
        <header className="py_1">
            <div className="main_width m_auto d_flex jus_space">
                <Link to='/' className='d_flex align_center gap-2'>
                    <img alt='Logo' src={Logo} width={60} />
                    <h2 className='font-xl'>CINEMA</h2>
                </Link>
                <ul className='d_flex align_center gap-2 color_white'>
                    <li><Link to='/'>Home</Link></li>
                    <li><Link to='/cinema'>Cinemas</Link></li>
                    <li><Link to='/all-movies'>Movies</Link></li>
                    <li className='user'>
                        {
                            name ?
                                <>
                                    <span>{name}</span>
                                    <ul className='p_1'>
                                        <li><Link to='/user'>Thông tin cá nhân</Link></li>
                                        <li><Link to='/' onClick={logOut}>Đăng xuất</Link></li>
                                    </ul>
                                </> : <Link to='/login'>Login</Link>
                        }
                    </li>
                </ul>
            </div>
        </header>
    )
};
