import './Footer.scss'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAnglesRight } from '@fortawesome/free-solid-svg-icons'
import { faSquareFacebook, faApple, faSquareYoutube, faInstagram, faGooglePlay } from '@fortawesome/free-brands-svg-icons'

export default function Footer() {
    return (
        <div className='MainFoot flex_col d_flex jus_center py_5'>
            <div className='main_width m_auto color_white py_3'>
                <div className='AboveBox d_flex jus_space'>
                    <div className='Footbox'>
                        <h3>GIỚI THỆU</h3>
                        <ul>
                            <li><a href='/'> <FontAwesomeIcon icon={faAnglesRight} /> VỀ CHÚNG TÔI</a></li>
                            <li><a href='/'> <FontAwesomeIcon icon={faAnglesRight} /> THOẢ THUẬN NGƯỜI DÙNG </a></li>
                            <li><a href='/'> <FontAwesomeIcon icon={faAnglesRight} /> QUY CHẾ HOẠT ĐỘNG</a></li>
                            <li><a href='/'> <FontAwesomeIcon icon={faAnglesRight} /> CHÍNH SÁCH BẢO MẬT </a></li>
                        </ul>
                    </div>
                    <div className='Footbox'>
                        <h3>GÓC ĐIỆN ẢNH</h3>
                        <ul>
                            <li><a href='/'> <FontAwesomeIcon icon={faAnglesRight} /> THỂ LOẠI PHIM</a></li>
                            <li><a href='/'> <FontAwesomeIcon icon={faAnglesRight} /> BÌNH LUẬN PHIM</a></li>
                            <li><a href='/'> <FontAwesomeIcon icon={faAnglesRight} /> BLOG ĐIỆN ẢNH</a></li>
                            <li><a href='/'> <FontAwesomeIcon icon={faAnglesRight} /> PHIM HAY THÁNG</a></li>
                        </ul>
                    </div>
                    <div className='Footbox'>
                        <h3>HỖ TRỢ</h3>
                        <ul>
                            <li><a href='/'> <FontAwesomeIcon icon={faAnglesRight} /> GÓP Ý</a></li>
                            <li><a href='/'> <FontAwesomeIcon icon={faAnglesRight} /> SALE & SERVICES</a></li>
                            <li><a href='/'> <FontAwesomeIcon icon={faAnglesRight} /> RẠP GIÁ VÉ</a></li>
                            <li><a href='/'> <FontAwesomeIcon icon={faAnglesRight} /> TUYỂN DỤNG</a></li>
                        </ul>
                    </div>
                    <div className='Footbox'>
                        <div className='Footbox_down'>
                            <h3>KẾT NỐI VỚI GALAXY CINEMA</h3>
                            <ul>
                                <li><a href='/'><FontAwesomeIcon icon={faSquareFacebook} className='icon-24' /></a></li>
                                <li><a href='/'><FontAwesomeIcon icon={faSquareYoutube} className='icon-24' /></a></li>
                                <li><a href='/'><FontAwesomeIcon icon={faInstagram} className='icon-24' /></a></li>
                            </ul>
                        </div>

                        <div className='Footbox_down'>
                            <h3>DOWNLOAD APP</h3>
                            <ul>
                                <li><a href='/'><FontAwesomeIcon icon={faApple} className='icon-24' /></a></li>
                                <li><a href='/'><FontAwesomeIcon icon={faGooglePlay} className='icon-24' /></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div >
    )
}
