import { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";

import ShowingMovie from "../Movie/ShowingMovie";
import ComingMovie from "../Movie/ComingMovie";
import Banner from "./Banner";
import Promotion from "../Promotion";
import './style.scss'

export default function Home() {
    const allMovies = useSelector((state: any) =>  state.AllMovies);
    const dispatch = useDispatch();
    const navigate = useNavigate()

    useEffect(() => {
        dispatch({ type: 'GET_ALL', payload: () => navigate('/maintenance') })
    }, [dispatch, navigate]);

    return (
        <div id='body'>
            <Banner movielist={allMovies.showingMovies} />
            <div id='showing-movie' className="py_3">
                <div className="main_width m_auto">
                    <div className="py_2 border-b">
                        <h2 className="color_orange">Phim đang chiếu</h2>
                    </div>
                    <ShowingMovie movielist={allMovies.showingMovies} />
                </div>
            </div>
            <div id='coming-movies' className="py_3">
                <div className="main_width m_auto color_white">
                    <div className="py_2 border-b">
                        <h2 className="color_orange">Phim sắp chiếu</h2>
                    </div>
                    <ComingMovie movielist={allMovies.comingMovies} />
                </div>
            </div>
            <div className="py_3">
                <Promotion />
            </div>
        </div>
    )
};
