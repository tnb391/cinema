import eMovie from "~/Components/Model/eMovie";
import MovieCard from "~/Components/Movie/MovieCard";

function ShowingMovie({movielist}:any) {  
    return (
        <div className="d_flex flex_wrap showing">
            {
                movielist.map((movie:eMovie) => {
                    return <div className="w_25 p_2" key={movie.id}>
                        <MovieCard movie={movie} />
                    </div>
                })
            }
        </div>
    )
};
export default ShowingMovie;
