import { useNavigate } from "react-router-dom"

export default function MovieCardLandscape({ movie }: any) {
    const navigate = useNavigate();
    const handleDirect = () => {
        navigate(`/detail/${movie.slug}`)
    }

    return (
        <div className='py_2 movie-card' onClick={handleDirect}>
            <img alt="illustration" src={movie.imageLandscape} className='w_100 rounded-sm' />
            <div className='movie-name'>
                <h3>{movie.name}</h3>
                <p className='color_gray'>{movie.subName}</p>
            </div>
        </div>
    )
};
