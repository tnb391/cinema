import { Component } from "react";
import Slider from 'react-slick';
import { Navigate } from "react-router-dom";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import './style.css'
import eMovie from "~/Components/Model/eMovie";
export default class ComingMovie extends Component<{ movielist: Array<eMovie> }> {
    state = { redirect: false, slug: "" };
    render() {
        let { redirect, slug } = this.state;
        const movielist: Array<eMovie> = this.props.movielist;
        const handleDirect = (slug: string) => {
            this.setState({ redirect: true, slug: slug });
        }
        const settings = {
            dots: true,
            infinite: true,
            speed: 500,
            slidesToShow: 4,
            slidesToScroll: 1
        };

        return (
            <>
                {redirect && <Navigate to={`/detail/${slug}`} replace={true} />}
                <Slider {...settings}>
                    {
                        movielist.map((item, index) => {
                            return (
                                <div className="p_2" key={index}>
                                    <div key={index} className='movie-card'>
                                        <img
                                            alt="illustration"
                                            src={item.imagePortrait}
                                            className='w_100 rounded-sm'
                                            onClick={() => handleDirect(item.slug)}
                                        />
                                        <div className="p_1">
                                            <h3 className="">{item.name}</h3>
                                            <p className="color_gray">{item.subName}</p>
                                        </div>
                                    </div>
                                </div>
                            )
                        })
                    }
                </Slider>
            </>
        );
    }
}