# Git Tutorial
dau tien phai git pull

1. **git status**: 
    Kiểm tra trạng thái của source (add/update/delete)

2. **git add .**: 
    Ghi nhớ trạng thái sửa đổi trong status

3. **git commit -m "message"**: 
    Xác nhận sửa đổi, theo ghi chú trong message (chỉ commit các file đã được add)

4. **git push**: 
    Đẩy các file đã commit lên gitlab